# README #

Example of MVVM, RxJAVA, Dagger 2 and Paging Library together.

### What is this repository for? ###

This is the example of how to implement MVVM design pattern using RxJava instead of LiveData, which is a small part of RxJava, with Dagger 2 dependency injection.
Also using Paging Library to load and display data in RecyclerView.

Both Java and Kotlin programming languages are used in this project.

### How do I get set up? ###

Following dependencies are included to run this project in Android Studio

//dagger2

implementation "com.google.dagger:dagger:2.21"

kapt "com.google.dagger:dagger-compiler:2.21"

//dagger 2 android

implementation "com.google.dagger:dagger-android:2.21"

implementation "com.google.dagger:dagger-android-support:2.21"

kapt "com.google.dagger:dagger-android-processor:2.21"

compileOnly 'javax.annotation:jsr250-api:1.0'

//RxJava2

implementation 'io.reactivex.rxjava2:rxjava:2.1.10'

implementation 'io.reactivex.rxjava2:rxandroid:2.0.0'

//Paging Library

def paging_version = "1.0.1"

implementation "android.arch.paging:runtime:$paging_version"

// optional - RxJava support

implementation "android.arch.paging:rxjava2:$paging_version"
	
//okhttp

implementation 'com.squareup.okhttp3:okhttp:3.9.1'	
	
Note: Following are the dependencies implementation for LiveData and ViewModel

(But in this project, RxJava is used instead of LiveData)

//LiveData and ViewModel

def lifecycle_version = "1.1.1"

implementation "android.arch.lifecycle:extensions:$lifecycle_version"

annotationProcessor "android.arch.lifecycle:compiler:$lifecycle_version"	