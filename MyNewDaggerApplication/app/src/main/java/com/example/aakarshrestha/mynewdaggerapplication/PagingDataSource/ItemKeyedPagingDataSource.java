package com.example.aakarshrestha.mynewdaggerapplication.PagingDataSource;

import android.arch.paging.ItemKeyedDataSource;
import android.support.annotation.NonNull;
import com.example.aakarshrestha.mynewdaggerapplication.Config.Config;
import okhttp3.*;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import javax.inject.Inject;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/*
    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
 */

/**
 * Created by Aakar Shrestha on April, 25, 2019
 */

public class ItemKeyedPagingDataSource extends ItemKeyedDataSource<String, String> {

    private OkHttpClient okHttpClient;
    private Request.Builder request;

    @Inject
    public ItemKeyedPagingDataSource(OkHttpClient okHttpClient, Request.Builder request) {
        this.okHttpClient = okHttpClient;
        this.request = request;
    }

    @Override
    public void loadInitial(@NonNull LoadInitialParams<String> params, @NonNull final LoadInitialCallback<String> callback) {

        Call call = okHttpClient.newCall(request.url(Config.PHOTOS_URL).build());
        call.enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {

            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {

                if (response.isSuccessful()) {

                    List<String> list = new ArrayList<>();

                    String mResponse = Objects.requireNonNull(response.body()).string();
                    try {
                        JSONArray jsonArray = new JSONArray(mResponse);

                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject object = jsonArray.getJSONObject(i);
                            String title = object.getString("title");

                            list.add(i+1 + ". "+title);
                        }

                        callback.onResult(list);


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }

            }
        });

    }

    @Override
    public void loadAfter(@NonNull LoadParams<String> params, @NonNull final LoadCallback<String> callback) {
        Call call = okHttpClient.newCall(request.url(Config.PHOTOS_URL).build());
        call.enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {

            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {

                if (response.isSuccessful()) {

                    List<String> list = new ArrayList<>();

                    String mResponse = Objects.requireNonNull(response.body()).string();
                    try {
                        JSONArray jsonArray = new JSONArray(mResponse);

                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject object = jsonArray.getJSONObject(i);
                            String title = object.getString("title");
                            list.add(i+1 + ". "+title);
                        }

                        callback.onResult(list);

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }

            }
        });
    }

    @Override
    public void loadBefore(@NonNull LoadParams<String> params, @NonNull LoadCallback<String> callback) {

    }

    @NonNull
    @Override
    public String getKey(@NonNull String item) {
        return item;
    }
}
