package com.example.aakarshrestha.mynewdaggerapplication.PagingDataSource;

import android.arch.paging.DataSource;
import javax.inject.Inject;

/**
 * Created by Aakar Shrestha on April, 25, 2019
 */

public class ItemKeyedPagingDataSourceFactory extends DataSource.Factory<String, String> {

    @Inject
    ItemKeyedPagingDataSource itemKeyedDataSource;

    @Inject
    public ItemKeyedPagingDataSourceFactory() {

    }

    @Override
    public DataSource<String, String> create() {

        return itemKeyedDataSource;
    }
}
