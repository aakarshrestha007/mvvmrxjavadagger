package com.example.aakarshrestha.mynewdaggerapplication.DataSource;

import com.example.aakarshrestha.mynewdaggerapplication.Config.Config;
import io.reactivex.subjects.BehaviorSubject;
import okhttp3.*;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import javax.inject.Inject;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * Created by Aakar Shrestha on April, 25, 2019
 */

/*
    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
 */

public class TitleDataSource {

    private BehaviorSubject<List<String>> listBehaviorSubject = BehaviorSubject.create();

    private OkHttpClient okHttpClient;

    private Request.Builder request;

    @Inject
    public TitleDataSource(OkHttpClient okHttpClient, Request.Builder request) {

        this.okHttpClient = okHttpClient;
        this.request = request;

    }

    public BehaviorSubject<List<String>> getDataFromNetwork() {
        return loadDataFromNetwork();
    }

    private BehaviorSubject<List<String>> loadDataFromNetwork() {

        Call call = okHttpClient.newCall(request.url(Config.PHOTOS_URL).build());
        call.enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {

            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {

                if (response.isSuccessful()) {

                    List<String> list = new ArrayList<>();

                    String mResponse = Objects.requireNonNull(response.body()).string();
                    try {
                        JSONArray jsonArray = new JSONArray(mResponse);
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject object = jsonArray.getJSONObject(i);
                            String title = object.getString("title");
                            list.add(title);
                        }

                        listBehaviorSubject.onNext(list);


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }

            }
        });

        return listBehaviorSubject;
    }


}
