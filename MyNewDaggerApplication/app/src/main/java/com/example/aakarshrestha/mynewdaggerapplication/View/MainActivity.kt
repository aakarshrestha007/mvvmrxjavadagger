package com.example.aakarshrestha.mynewdaggerapplication.View

import android.arch.lifecycle.ViewModelProviders
import android.arch.paging.PagedList
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.util.DiffUtil
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.LinearLayoutManager
import com.example.aakarshrestha.mynewdaggerapplication.Adapter.TitleRecyclerViewAdapter
import com.example.aakarshrestha.mynewdaggerapplication.DependencyInjection.Components.DaggerMagicBoxComponent
import com.example.aakarshrestha.mynewdaggerapplication.Model.Info
import com.example.aakarshrestha.mynewdaggerapplication.R
import com.example.aakarshrestha.mynewdaggerapplication.ViewModel.PagingViewModel
import com.example.aakarshrestha.mynewdaggerapplication.ViewModel.TitleViewModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_main.*

import javax.inject.Inject

class MainActivity : AppCompatActivity() {

    @Inject
    lateinit var info: Info
    @Inject
    lateinit var compositeDisposable: CompositeDisposable

    lateinit var titleViewModel: TitleViewModel
    lateinit var disposable: Disposable

    lateinit var daggerMagicBoxComponent: DaggerMagicBoxComponent
    lateinit var pagingViewModel: PagingViewModel

    lateinit var linearLayoutManager: LinearLayoutManager
    lateinit var titleRecyclerViewAdapter: TitleRecyclerViewAdapter

    lateinit var titlePagedList: PagedList<String>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        //Initialize DaggerComponent
        daggerMagicBoxComponent = DaggerMagicBoxComponent.builder().build() as DaggerMagicBoxComponent
        daggerMagicBoxComponent.inject(this)

        //Setup the top title
        titleViewModel = ViewModelProviders.of(this).get(TitleViewModel::class.java)
        helloWorld.text = info.text

        //Setup LinearLayoutManager to Recyclerview
        linearLayoutManager = LinearLayoutManager(this)
        recyclerView.layoutManager = linearLayoutManager

        //Initialize PagingViewModel
        pagingViewModel = ViewModelProviders.of(this).get(PagingViewModel::class.java)

        //Call this function to load data in the background
        getPagingData()

    }

    private fun getPagingData() {

        disposable = pagingViewModel.titlePagedList
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe { onNext ->
                titlePagedList = onNext

                showOnRecyclerView()
            }

        compositeDisposable.add(disposable)

    }

    private fun showOnRecyclerView() {
        titleRecyclerViewAdapter = TitleRecyclerViewAdapter(CALLBACK, this@MainActivity)
        titleRecyclerViewAdapter.submitList(titlePagedList)

        recyclerView.itemAnimator = DefaultItemAnimator()
        recyclerView.adapter = titleRecyclerViewAdapter
        titleRecyclerViewAdapter.notifyDataSetChanged()
    }

    companion object {
        val CALLBACK = object : DiffUtil.ItemCallback<String>() {
            override fun areItemsTheSame(oldItem: String?, newItem: String?): Boolean {
                return oldItem.toString() == newItem.toString()
            }

            override fun areContentsTheSame(oldItem: String?, newItem: String?): Boolean {
                return true
            }
        }
    }

    override fun onDestroy() {
        super.onDestroy()

        if (!compositeDisposable.isDisposed) {
            compositeDisposable.dispose()
        }
    }
}
