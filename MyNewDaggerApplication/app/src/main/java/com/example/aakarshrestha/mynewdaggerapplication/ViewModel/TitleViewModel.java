package com.example.aakarshrestha.mynewdaggerapplication.ViewModel;

import android.arch.lifecycle.ViewModel;
import com.example.aakarshrestha.mynewdaggerapplication.DataSource.TitleDataSource;
import com.example.aakarshrestha.mynewdaggerapplication.DependencyInjection.Components.DaggerMagicBoxComponent;
import io.reactivex.subjects.BehaviorSubject;

import javax.inject.Inject;
import java.util.List;

/**
 * Created by Aakar Shrestha on April, 25, 2019
 */

/*
    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
 */

public class TitleViewModel extends ViewModel {

    private BehaviorSubject<List<String>> data;

    @Inject
    TitleDataSource titleDataSource;

    public TitleViewModel() {

        DaggerMagicBoxComponent daggerMagicBoxComponent = (DaggerMagicBoxComponent) DaggerMagicBoxComponent.builder().build();
        daggerMagicBoxComponent.inject(this);

        data = titleDataSource.getDataFromNetwork();

    }

    public BehaviorSubject<List<String>> getData() {

        if (data == null) {
            data = BehaviorSubject.create();
        }

        return data;

    }
}
