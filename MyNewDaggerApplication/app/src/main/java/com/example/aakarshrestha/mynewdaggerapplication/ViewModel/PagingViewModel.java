package com.example.aakarshrestha.mynewdaggerapplication.ViewModel;

import android.arch.lifecycle.ViewModel;
import android.arch.paging.PagedList;
import android.arch.paging.RxPagedListBuilder;
import com.example.aakarshrestha.mynewdaggerapplication.DependencyInjection.Components.DaggerMagicBoxComponent;
import com.example.aakarshrestha.mynewdaggerapplication.PagingDataSource.ItemKeyedPagingDataSourceFactory;
import io.reactivex.Observable;

import javax.inject.Inject;

/*
    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
 */

/**
 * Created by Aakar Shrestha on April, 25, 2019
 */

public class PagingViewModel extends ViewModel {

    private Observable<PagedList<String>> mObservableTitleListData;

    @Inject
    ItemKeyedPagingDataSourceFactory factory;

    public PagingViewModel() {

        DaggerMagicBoxComponent daggerMagicBoxComponent = (DaggerMagicBoxComponent) DaggerMagicBoxComponent.builder().build();
        daggerMagicBoxComponent.inject(this);

        PagedList.Config config = (new PagedList.Config.Builder())
                .setEnablePlaceholders(true)
                .setInitialLoadSizeHint(10)
                .setPageSize(1)
                .build();

        mObservableTitleListData = new RxPagedListBuilder<>(factory, config)
                .buildObservable();

    }

    public Observable<PagedList<String>> getTitlePagedList() {
        return mObservableTitleListData;
    }
}
