package com.example.aakarshrestha.mynewdaggerapplication.Model

import javax.inject.Inject

/**
 * Created by Aakar Shrestha on April, 24, 2019
 */

class Info @Inject constructor(){

    val text = "Hello From MVVM, Dagger 2 and RxJava"

}