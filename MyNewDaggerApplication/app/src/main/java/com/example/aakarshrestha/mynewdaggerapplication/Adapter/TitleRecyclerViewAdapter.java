package com.example.aakarshrestha.mynewdaggerapplication.Adapter;

import android.arch.paging.PagedListAdapter;
import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.v7.util.DiffUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.example.aakarshrestha.mynewdaggerapplication.R;
import com.example.aakarshrestha.mynewdaggerapplication.databinding.TitleListItemBinding;

/**
 * Created by Aakar Shrestha on April, 26, 2019
 */

public class TitleRecyclerViewAdapter extends PagedListAdapter<String, TitleRecyclerViewAdapter.TitleViewHolder> {

    private Context context;

    public TitleRecyclerViewAdapter(@NonNull DiffUtil.ItemCallback<String> diffCallback, Context context) {
        super(diffCallback);
        this.context = context;
    }

    @NonNull
    @Override
    public TitleViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        TitleListItemBinding titleListItemBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.title_list_item, parent, false);

        return new TitleViewHolder(titleListItemBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull TitleViewHolder holder, int position) {

        String value = getItem(position);
        holder.titleListItemBinding.titleTextViewID.setText(value);
    }

    class TitleViewHolder extends RecyclerView.ViewHolder {

        private TitleListItemBinding titleListItemBinding;

        TitleViewHolder(@NonNull TitleListItemBinding titleListItemBinding) {
            super(titleListItemBinding.getRoot());
            this.titleListItemBinding = titleListItemBinding;

            titleListItemBinding.getRoot().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                }
            });
        }
    }

}
